/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)

require('../scss/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
var $ = require('jquery');
//require('./modules/tabs');
//tabs.init();
//= modules/tabs.js
import * as toastr from "toastr";

import 'es6-promise/auto';

import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Vuex from 'vuex';
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload);
Vue.use(VueAxios, axios);
Vue.use(Vuex);

import Index from './components/Index'
import Login from './components/Login'
import Register from './components/Register'
import ForgetPassword from './components/ForgetPassword'
import ResetPassword from './components/ResetPassword'
import Profile from './components/Profile'
import Avatar from './components/Avatar'
import Uploader from './components/Uploader';


const store = new Vuex.Store({
    state: {
        authState: 'login',
    },

    mutations: {
        setAuthState (authState, val) {
            this.state.authState = val;
        }
    }
});

new Vue({
    el: '#app',
    store,
    components: {
        Index,
        Login,
        Register,
        ForgetPassword,
        ResetPassword,
        Profile,
        Avatar,
        Uploader
    },
    mounted() {
    },
});

$('.js-activate-education').click(function (e) {
    e.preventDefault();
    let $this = $(this);
    $.ajax({
        type: "POST",
        url: $this.data('url'),
        data: {id: $this.data('id')},
        success: function (response) {
            $this.closest('.js-course-status').text(response.message);
        },
    });
});

$('.js-tabs').each(function() {
    var $tabsWrap = $(this),
        $tabLinks = $tabsWrap.find('.js-tabs__link');

    $tabLinks.click(function(e){
        e.preventDefault();
        var $tabLinkToSwitch   = $(this);
        var
            $tabsWrap    = $tabLinkToSwitch.closest('.js-tabs'),
            $tabLinks    = $tabsWrap.find('.js-tabs__link'),
            $tabs 	     = $tabsWrap.find('.js-tabs__tab');

        $tabLinks.removeClass('active');
        $tabLinkToSwitch.addClass('active');
        $tabs.removeClass('active');
        $tabs.eq($tabLinkToSwitch.index()).addClass('active');
    });
});
$('.js-form-answer-init').submit(function (e) {
    e.preventDefault();
    let $form = $(this);
    let data = new FormData($form[0]);

    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: data,
        contentType: false,
        processData: false,
        success: function (response) {
            $('.js-form').show();
            $('.js-form-answer-init').hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.error(JSON.parse(xhr.responseText).message);
        }

    });
});
$('.js-form').submit(function (e) {
    e.preventDefault();
    let $form = $(this);
    let data = new FormData($form[0]);
    let $textarea = $(this).find('.js-textarea');
    let text = $textarea.val();
    if(text.length < 1){
        $textarea.addClass('error');
        return;
    } else {
        $textarea.removeClass('error');
        $textarea.addClass('success');
    }
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: data,
        contentType: false,
        processData: false,
        success: function (response) {
            //toastr.success(response.message);
            $('.js-alerts-container').html('<div class="alert alert--info">\n' +
                '<h3>Задание отправлено на проверку!</h3>\n' +
                '</div>');
            /*if(typeof response.images !=='undefined' ){
                $('.js-images').html('');
                $.each(response.images, function(index, value){
                    $form.find('.js-images').append('<img class="task-content__image" src="'+ value +'">');
                });
            }*/
        },
        error: function (xhr, ajaxOptions, thrownError) {
            toastr.error(JSON.parse(xhr.responseText).message);
        }
    });
});
/*$('.js-form').submit(function (e) {
    e.preventDefault();
    let $form = $(this);
    let data = new FormData($form[0]);

    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: data,
        contentType: false,
        processData: false,
        success: function (response) {
            toastr.success(response.message);
            $('.js-alerts-container').html('<div class="alert alert--info">\n' +
                '<h3>Задание отправлено на проверку.</h3>\n' +
                '<p class="alert__text">Вы можете редактировать ответ до тех пор, пока наставник с ним не ознакомится.</p>\n' +
                '</div>');
            if(typeof response.images !=='undefined' ){
                $('.js-images').html('');
                $.each(response.images, function(index, value){
                    $form.find('.js-images').append('<img class="task-content__image" src="'+ value +'">');
                });
            }
        }
    });
});
var inputs = document.querySelectorAll( '.js-inputfile' );
Array.prototype.forEach.call(inputs, function(input)
{
    var label	 = input.nextElementSibling,
        labelVal = label.innerHTML;
    input.addEventListener( 'change', function(e){
        var fileName = '';
        if( this.files && this.files.length > 1 )
            fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
        else
            fileName = e.target.value.split( '\\' ).pop();

        if( fileName )
            label.querySelector( 'span' ).innerHTML = fileName;
        else
            label.innerHTML = labelVal;
    });
});
$('.js-avatar').click(function (e) {
    e.preventDefault();
});
*/

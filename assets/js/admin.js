/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)


require('../scss/admin.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
//import $ from 'jquery';

import bootstrap from 'bootstrap';
import metismenu from 'metismenu';
import jstree from 'jstree';
import lightbox2 from 'lightbox2';
import * as toastr from 'toastr';
import * as floatthead from 'floatthead';

$(".js-metismenu").metisMenu();

$('.js-tree').jstree().bind("select_node.jstree", function (e, data) {
    var href = data.node.a_attr.href;
    var parentId = data.node.a_attr.parent_id;
    if(href == '#')
        return '';
    window.location.href = href;
});

$('.js-change-status').click(function (e) {
    e.preventDefault();
    let $parent = $(this).closest('.js-dropdown'),
        $dropdownLink = $parent.find('.js-dropdown-toggle'),
        url = $(this).data('url'),
        status = $(this).data('status')
    ;
    $dropdownLink.html($(this).find('.js-icon-wrapper').html());
    $.ajax({
        url: url,
        data: {status:status},
        method: "POST",
        success: function (response) {
            toastr.success(response.message);
        },
    });
});
$('.js-progress').floatThead({
    //zIndex: 2
});
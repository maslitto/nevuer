<?php

namespace App\Repository;

use App\Entity\ActivationRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ActivationRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActivationRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActivationRequest[]    findAll()
 * @method ActivationRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivationRequestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ActivationRequest::class);
    }

    // /**
    //  * @return ActivationRequest[] Returns an array of ActivationRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ActivationRequest
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getActivationRequestsQueryBuilder(array $filter) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('ar');
        $qb = $queryBuilder->addSelect('ar');
        if(isset($filter['query'])){
            $qb->leftJoin('\App\Entity\User\User', 'u',   'WITH',  'u.id = ar.user')
                ->where('u.email LIKE :query')
                ->setParameter('query', '%'.$filter['query'].'%');
        }
        return $qb->orderBy('ar.active','ASC')->orderBy('ar.createdAt','DESC');
    }


    public function getActivationRequestsPaginated(array $filter, int $pageNumber)
    {
        $qb = $this->getActivationRequestsQueryBuilder($filter);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $pageNumber, ActivationRequest::PER_PAGE);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function getNewRequestsCount()
    {
        $queryBuilder = $this->createQueryBuilder('ar');
        $qb = $queryBuilder->select('COUNT(ar.active)')->where('ar.active = 0');
        return $qb->getQuery()->getSingleScalarResult();
    }
}

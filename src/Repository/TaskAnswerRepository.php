<?php

namespace App\Repository;

use App\Entity\TaskAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TaskAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskAnswer[]    findAll()
 * @method TaskAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskAnswerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TaskAnswer::class);
    }

    // /**
    //  * @return TaskAnswer[] Returns an array of TaskAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaskAnswer
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getTaskAnswersQueryBuilder(array $filter) : QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('ta');
        $qb = $queryBuilder->addSelect('ta');
        $qb->leftJoin('\App\Entity\Task', 't',   'WITH',  't.id = ta.task');
        $qb->leftJoin('\App\Entity\Course', 'c',   'WITH',  'c.id = t.course');
        $qb->andWhere('c.active = true');
        if(isset($filter['query'])){
            $qb
                ->andWhere('ta.email LIKE :query')
                ->setParameter('query', '%'.$filter['query'].'%');
        }
        if(isset($filter['course'])){
            $qb
                ->andWhere('t.course = :course')
                ->setParameter('course', (int)$filter['course']);
        }
        if(isset($filter['taskId'])){
            $qb
                ->andWhere('t.id = :taskId')
                ->setParameter('taskId', (int) $filter['taskId']);
        }
        if(isset($filter['new'])){
            $qb
                ->andWhere('ta.adminNew = :new')
                ->setParameter('new', (bool)$filter['new']);
        }
        return $qb->orderBy('ta.updatedAt','DESC');
    }


    public function getTaskAnswersPaginated(array $filter, int $pageNumber)
    {
        $qb = $this->getTaskAnswersQueryBuilder($filter);
        $query = $qb->getQuery();
        return $this->createPaginator($query, $pageNumber, TaskAnswer::PER_PAGE);
    }

    private function createPaginator(Query $query, int $page, int $perPage): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($perPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190205144230 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE task_answer ADD editor_id INT DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE task_answer ADD CONSTRAINT FK_E174CEF68DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE task_answer ADD CONSTRAINT FK_E174CEF66995AC4C FOREIGN KEY (editor_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_E174CEF66995AC4C ON task_answer (editor_id)');
        $this->addSql('ALTER TABLE activation_request DROP FOREIGN KEY activation_request_ibfk_2');
        $this->addSql('ALTER TABLE activation_request DROP FOREIGN KEY activation_request_ibfk_3');
        $this->addSql('ALTER TABLE activation_request CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE activation_request ADD CONSTRAINT FK_234B4D3AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE activation_request ADD CONSTRAINT FK_234B4D3A591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY task_ibfk_4');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY task_ibfk_5');
        $this->addSql('ALTER TABLE task CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25C86F3B2F FOREIGN KEY (week_id) REFERENCES week (id)');
        $this->addSql('ALTER TABLE week DROP FOREIGN KEY week_ibfk_1');
        $this->addSql('DROP INDEX UNIQ_5B5A69C096901F54 ON week');
        $this->addSql('ALTER TABLE week CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE week ADD CONSTRAINT FK_5B5A69C0591CC992 FOREIGN KEY (course_id) REFERENCES course (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5B5A69C096901F54 ON week (number)');
        $this->addSql('ALTER TABLE answer_image DROP FOREIGN KEY answer_image_ibfk_1');
        $this->addSql('ALTER TABLE answer_image CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE answer_image ADD CONSTRAINT FK_5E07ED0CAA334807 FOREIGN KEY (answer_id) REFERENCES task_answer (id)');
        $this->addSql('ALTER TABLE course CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE user CHANGE hash hash VARCHAR(60) NOT NULL, CHANGE roles roles JSON NOT NULL, CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE user_profile CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE activation_request DROP FOREIGN KEY FK_234B4D3AA76ED395');
        $this->addSql('ALTER TABLE activation_request DROP FOREIGN KEY FK_234B4D3A591CC992');
        $this->addSql('ALTER TABLE activation_request CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE activation_request ADD CONSTRAINT activation_request_ibfk_2 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activation_request ADD CONSTRAINT activation_request_ibfk_3 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE answer_image DROP FOREIGN KEY FK_5E07ED0CAA334807');
        $this->addSql('ALTER TABLE answer_image CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE answer_image ADD CONSTRAINT answer_image_ibfk_1 FOREIGN KEY (answer_id) REFERENCES task_answer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25591CC992');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25C86F3B2F');
        $this->addSql('ALTER TABLE task CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT task_ibfk_4 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT task_ibfk_5 FOREIGN KEY (week_id) REFERENCES week (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE task_answer DROP FOREIGN KEY FK_E174CEF68DB60186');
        $this->addSql('ALTER TABLE task_answer DROP FOREIGN KEY FK_E174CEF66995AC4C');
        $this->addSql('DROP INDEX IDX_E174CEF66995AC4C ON task_answer');
        $this->addSql('ALTER TABLE task_answer DROP editor_id, CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE task_answer ADD CONSTRAINT task_answer_ibfk_1 FOREIGN KEY (task_id) REFERENCES task (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user CHANGE hash hash VARCHAR(60) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE roles roles VARCHAR(255) NOT NULL COLLATE utf8_general_ci, CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE user_profile CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE week DROP FOREIGN KEY FK_5B5A69C0591CC992');
        $this->addSql('DROP INDEX UNIQ_5B5A69C096901F54 ON week');
        $this->addSql('ALTER TABLE week CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE week ADD CONSTRAINT week_ibfk_1 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX UNIQ_5B5A69C096901F54 ON week (number)');
    }
}

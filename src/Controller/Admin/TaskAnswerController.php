<?php


namespace App\Controller\Admin;


use App\Entity\Task;
use App\Entity\TaskAnswer;
use App\Entity\User\User;
use App\Entity\Week;
use App\Repository\CourseRepository;
use App\Repository\TaskAnswerRepository;
use App\Repository\TaskRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted({"ROLE_ADMIN","ROLE_EDITOR"})
 * Admin TaskAnswer controller
 * @Route("/admin/task-answer")
*/

class TaskAnswerController extends AbstractController
{

    /**
     * @Route("/", name="admin_task_answer_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(
        TaskAnswerRepository $taskAnswerRepository,
        TaskRepository $taskRepository,
        Request $request,
        CourseRepository $courseRepository)
    {
        $courses = $courseRepository->findBy(['active' => true]);
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $taskId = $request->query->get('taskId',NULL);
        $taskAnswers = $taskAnswerRepository->getTaskAnswersPaginated($filter, $page);
        $return = [
            'taskAnswers' => $taskAnswers,
            'courses' => $courses
        ];
        if($taskId){
            $task = $taskRepository->findOneBy(['id' => $taskId]);
            $return['task'] = $task;
        }

        return $this->render('admin/task-answer/index.html.twig', $return);
    }

    /**
     * @Route("/edit/{id}/", name="admin_task_answer_edit")
     * @Method({"GET","POST"})
     *
     */
    public function edit(TaskAnswer $taskAnswer, Request $request )
    {
        $form = $this->createFormBuilder($taskAnswer)
            /*->add('task', EntityType::class, array(
                'class' => Task::class,
                'choice_label' => function ($choiceValue, $key, $value) {
                    return 'Тут ничего не меняем.  (Неделя '.$choiceValue->getWeek()->getNumber().' ,'.$choiceValue->getDay().')';
                },
                'label' => 'Задание',
                'required' => true
            ))*/
            ->add('text', TextAreaType::class,['label'=>'Ответ пользователя','required' => false,'attr'=>['rows' => 15]])
            ->add('comment', TextAreaType::class,['label'=>'Комментарий наставника','required' => false,'attr' => ['class' => 'js-ckeditor']])
            ->add('status', ChoiceType::class,[
                'label'=>'Статус',
                'required' => true,
                'choices' => [
                    'На рассмотрении' => 0,
                    'Зачёт' => 1,
                    'Не зачёт' => 2,
                    'На доработку' => 3,
                ]
            ])
            ->add('adminNew', HiddenType::class,['attr'=>['value' => 0]])
            ->add('userNew', HiddenType::class,['attr'=>['value' => 1]])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $taskAnswer = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $taskAnswer->setEditor($this->getUser());
            $entityManager->persist($taskAnswer);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_task_answer_edit',['id' => $taskAnswer->getId()]);
        }
        return $this->render('admin/task-answer/edit.html.twig',[
            'form' => $form->createView(),
            'taskAnswer' => $taskAnswer,
        ]);
    }


}

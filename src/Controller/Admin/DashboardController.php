<?php


namespace App\Controller\Admin;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
/**
 * Admin dashboard controller
 * @IsGranted({"ROLE_ADMIN","ROLE_EDITOR"})
 * @Route("/admin")
*/

class DashboardController extends AbstractController
{

    /**
     * @Route("/", name="admin_dashboard_page_index")
     */
    public function index()
    {
        return $this->render('admin/dashboard/dashboard.html.twig',[

        ]);
    }

}

<?php


namespace App\Controller\Admin;


use App\Entity\ActivationRequest;
use App\Entity\Course;
use App\Entity\User\User;
use App\Form\CkfinderType;
use App\Repository\ActivationRequestRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * Admin ActivationRequest controller
 * @Route("/admin/actvation-request")
*/

class ActivationRequestController extends AbstractController
{

    /**
     * @Route("/", name="admin_activation_request_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(ActivationRequestRepository $activationRequestRepository,Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $activationRequests = $activationRequestRepository->getActivationRequestsPaginated($filter, $page);
        return $this->render('admin/activation-request/index.html.twig',[
            'activationRequests' => $activationRequests,
        ]);
    }
    /**
     * @Route("/activate/{id}/", name="admin_activation_request_activate")
     */
    public function activate(int $id, ActivationRequestRepository $activationRequestRepository)
    {
        /** @var ActivationRequest $activationRequest */
        $activationRequest = $activationRequestRepository->findOneBy(['id' => $id ]);

        /** @var Course $course */
        $course = $activationRequest->getCourse();
        /** @var User $user */
        $user = $activationRequest->getUser();
        $course->addUser($user);
        $user->addCourse($course);
        $activationRequest->setActive(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($activationRequest);
        $em->persist($course);
        $em->persist($user);
        $em->flush();

        $this->addFlash('success','Успешная активация');
        return $this->redirectToRoute('admin_activation_request_index');
    }
    /**
     * @Route("/deactivate/{id}/", name="admin_activation_request_deactivate")
     */
    public function deactivate(int $id, ActivationRequestRepository $activationRequestRepository)
    {
        /** @var ActivationRequest $activationRequest */
        $activationRequest = $activationRequestRepository->findOneBy(['id' => $id ]);

        /** @var Course $course */
        $course = $activationRequest->getCourse();
        /** @var User $user */
        $user = $activationRequest->getUser();
        $course->removeUser($user);
        $user->removeCourse($course);
        $activationRequest->setActive(false);

        $em = $this->getDoctrine()->getManager();
        $em->persist($activationRequest);
        $em->persist($course);
        $em->persist($user);
        $em->flush();

        $this->addFlash('success','Успешная деактивация');
        return $this->redirectToRoute('admin_activation_request_index');
    }

    /**
     * @Route("/delete/{id}/", name="admin_activation_request_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, ActivationRequestRepository $activationRequestRepository)
    {
        $activationRequest = $activationRequestRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($activationRequest);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_activation_request_index');
    }

}

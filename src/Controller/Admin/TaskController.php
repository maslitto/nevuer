<?php


namespace App\Controller\Admin;


use App\Entity\Course;
use App\Entity\Task;
use App\Entity\Week;
use App\Form\CkfinderType;
use App\Repository\CourseRepository;
use App\Repository\TaskRepository;
use App\Repository\WeekRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * Admin Task controller
 * @Route("/admin/task")
*/

class TaskController extends AbstractController
{

    /**
     * @Route("/", name="admin_task_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(TaskRepository $taskRepository,Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $tasks = $taskRepository->getTasksPaginated($filter, $page);
        return $this->render('admin/task/index.html.twig',[
            'tasks' => $tasks,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_task_edit")
     * @Route("/add/", name="admin_task_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, TaskRepository $taskRepository)
    {
        if($id){
            $task = $taskRepository->findOneBy(['id' => $id]);
        } else{
            $task = new Task();
        }
        $form = $this->createFormBuilder($task)
            ->add('course', EntityType::class, array(
                'class' => Course::class,
                'choice_label' => 'title',
                'label' => 'Курс',
                'required' => true,
                'query_builder' => function (CourseRepository $cr) {
                    return $cr->createQueryBuilder('c')
                        ->andWhere('c.active = 1');
                },
            ))
            ->add('week', EntityType::class, array(
                'class' => Week::class,
                'choice_label' => function ($choiceValue, $key, $value) {
                    return $choiceValue->getCourse()->getTitle().': Неделя '. $choiceValue->getNumber();
                },
                'query_builder' => function (WeekRepository $wr) {
                    return $wr->createQueryBuilder('w')
                        ->leftJoin('\App\Entity\Course', 'c',   'WITH',  'c.id = w.course')
                        ->andWhere('c.active = 1');
                },
                'label' => 'Неделя',
                'required' => true
            ))
            ->add('title', TextType::class,['label'=>'Название','required' => false])
            ->add('text', TextAreaType::class,['label'=>'Описание','required' => false,'attr' => ['class' => 'js-ckeditor']])
            ->add('recommendation', TextAreaType::class,['label'=>'Рекомендация наставника','required' => false,'attr' => ['class' => 'js-ckeditor']])
            ->add('deadline', DateTimeType::class,['label'=>'Дедлайн','required' => true])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_task_edit',['id' => $task->getId()]);
        }
        return $this->render('admin/task/edit.html.twig',[
            'form' => $form->createView(),
            'task' => $task
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_task_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, TaskRepository $taskRepository)
    {
        $task = $taskRepository->findOneBy(['id' => $id]);
        $weekId = $task->getWeek()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($task);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_week_edit',['id' => $weekId]);
    }

}

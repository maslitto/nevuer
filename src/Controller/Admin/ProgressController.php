<?php


namespace App\Controller\Admin;


use App\Entity\Course;
use App\Entity\Task;
use App\Entity\TaskAnswer;
use App\Entity\User\User;
use App\Form\CkfinderType;
use App\Repository\CourseRepository;
use League\Flysystem\Exception;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * Admin Course controller
 * @Route("/admin/progress")
*/

class ProgressController extends AbstractController
{

    /**
     * @Route("/{course}", name="admin_progress_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(Request $request, Course $course)
    {
        return $this->render('admin/progress/index.html.twig',[
            'course' => $course,
        ]);
    }

    /**
     * @Route("/change-task-answer-status/{task}/{user}", name="admin_progress_edit")
     * @Method("POST")
     *
     */
    public function changeTaskAnswerStatus(Task $task, User $user, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $status = $request->get('status');
        if($answer = $task->getAnswerFromUser($user)){
            $answer->setStatus($status);
        } else {
            $answer = new TaskAnswer();
            $answer->setStatus($status);
            $answer->setUser($user);
            $answer->setTask($task);
        }
        $em->persist($answer);
        $em->flush();

        return $this->json([
            'message' => 'Успешное изменение статуса!'
        ]);
    }
}

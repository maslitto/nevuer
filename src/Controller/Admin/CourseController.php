<?php


namespace App\Controller\Admin;


use App\Entity\Course;
use App\Entity\Task;
use App\Entity\Week;
use App\Form\CkfinderType;
use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * Admin Course controller
 * @Route("/admin/course")
*/

class CourseController extends AbstractController
{

    /**
     * @Route("/", name="admin_course_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(CourseRepository $courseRepository,Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $courses = $courseRepository->getCoursesPaginated($filter, $page);
        return $this->render('admin/course/index.html.twig',[
            'courses' => $courses,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="admin_course_edit")
     * @Route("/add/", name="admin_course_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, CourseRepository $courseRepository)
    {
        if($id){
            $course = $courseRepository->findOneBy(['id' => $id]);
        } else{
            $course = new Course();
        }
        $form = $this->createFormBuilder($course)
            ->add('active', CheckboxType::class,['label'=>'Активный','required' => false])
            ->add('dateStart', DateType::class,['label'=>'Дата начала','required' => true])
            ->add('title', TextType::class,['label'=>'Название','required' => false])
            ->add('teachers', TextType::class,['label'=>'Наставник(и)','required' => false])
            ->add('link', TextType::class,['label'=>'Ссылка','required' => false])
            //->add('image', TextType::class,['label'=>'Картинка','required' => false])
            ->add('image', CkfinderType::class,['label'=>'Путь к фото','required' => false])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $course = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($course);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_course_edit',['id' => $course->getId()]);
        }

        return $this->render('admin/course/edit.html.twig',[
            'form' => $form->createView(),
            'course' => $course
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_course_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, CourseRepository $courseRepository)
    {
        $course = $courseRepository->findOneBy(['id' => $id]);
        $entityManager = $this->getDoctrine()->getManager();

        $weeks = $course->getWeeks();
        /** @var Week $week */
        foreach ($weeks as $week){
            /** @var Task $task */
            foreach ($week->getTasks() as $task){
                $answers = $task->getAnswers();
                foreach ($answers as $answer) {
                    $images = $answer->getAnswerImages();
                    foreach ($images as $image) {
                        $entityManager->remove($image);
                    }
                    $entityManager->remove($answer);
                }
                $entityManager->remove($task);
            }
            $entityManager->remove($week);
        }
        $entityManager->remove($course);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_course_index');
    }
    /**
     * @Route("/copy/{course}/", name="admin_course_copy")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function copy(Course $course)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $newCourse = clone $course;
        $newCourse->setCreatedAt(new \DateTime());
        $newCourse->setUpdatedAt(new \DateTime());
        foreach ($course->getWeeks() as $week) {
            /** @var Week $newWeek */
            $newWeek = clone $week;
            $newWeek->setCreatedAt(new \DateTime());
            $newWeek->setUpdatedAt(new \DateTime());
            $newWeek->setCourse($newCourse);
            foreach ($week->getTasks() as $task){
                /** @var Task $newTask */
                $newTask = clone $task;
                $newTask->setCreatedAt(new \DateTime());
                $newTask->setUpdatedAt(new \DateTime());
                $newTask->setCourse($newCourse);
                $newTask->setWeek($newWeek);
                $entityManager->persist($newTask);
            }
            $entityManager->persist($newWeek);
        }
        $entityManager->persist($newCourse);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно скопирована');
        return $this->redirectToRoute('admin_course_index');
    }

}

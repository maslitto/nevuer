<?php


namespace App\Controller\Admin;


use App\Entity\Course;
use App\Entity\Task;
use App\Entity\TaskAnswer;
use App\Entity\Week;
use App\Form\CkfinderType;
use App\Repository\CourseRepository;
use App\Repository\WeekRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 * Admin Week controller
 * @Route("/admin/week")
*/

class WeekController extends AbstractController
{

    /**
     * @Route("/", name="admin_week_index")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function index(WeekRepository $weekRepository,Request $request)
    {
        $filter = $request->query->all();
        $page = $request->query->get('page',1);
        $weeks = $weekRepository->getWeeksPaginated($filter, $page);
        /*return $this->render('admin/week/index.html.twig',[
            'weeks' => $weeks,
        ]);*/
    }


    /**
     * @Route("/edit/{id}/", name="admin_week_edit")
     * @Route("/add/", name="admin_week_add")
     * @Method({"GET","POST"})
     *
     */
    public function edit(Request $request, int $id = NULL, WeekRepository $weekRepository)
    {
        if($id){
            $week = $weekRepository->findOneBy(['id' => $id]);
        } else{
            $week = new Week();
        }
        for($i = 0;$i <= 10; $i++){
            $choices[$i] = $i;
        }
        $form = $this->createFormBuilder($week)
            ->add('course', EntityType::class, array(
                'class' => Course::class,
                'choice_label' => function ($choiceValue, $key, $value) {
                    return $choiceValue->getTitle().': (Дата начала '. $choiceValue->getDateStart()->format('d-m-Y').')';
                },
                'label' => 'Курс',
                'query_builder' => function (CourseRepository $cr) {
                    return $cr->createQueryBuilder('c')
                        ->andWhere('c.active = 1');
                },
            ))
            ->add('number', ChoiceType::class,[
                'choices'  => $choices,
                'label'=>'Номер недели'])
            ->add('title', TextType::class,['label'=>'Название','required' => false])
            ->add('text', TextAreaType::class,['label'=>'Описание','required' => false,'attr' => ['class' => 'js-ckeditor']])
            ->add('save', SubmitType::class, ['label' => 'Сохранить','attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $week = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($week);
            $entityManager->flush();
            $this->addFlash('success','Успешное сохранение записи');
            return $this->redirectToRoute('admin_week_edit',['id' => $week->getId()]);
        }
        return $this->render('admin/week/edit.html.twig',[
            'form' => $form->createView(),
            'week' => $week
        ]);
    }

    /**
     * @Route("/delete/{id}/", name="admin_week_delete")
     * @Method("GET")
     * @Cache(smaxage="10")
     *
     */
    public function delete(int $id, WeekRepository $weekRepository)
    {
        $week = $weekRepository->findOneBy(['id' => $id]);
        $courseId = $week->getCourse()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $tasks = $week->getTasks();
        /** @var Task $task */
        foreach ($tasks as $task) {
            $answers = $task->getAnswers();
            foreach ($answers as $answer) {
                $images = $answer->getAnswerImages();
                foreach ($images as $image) {
                    $entityManager->remove($image);
                }
                $entityManager->remove($answer);
            }
            $entityManager->remove($task);
        }
        $entityManager->remove($week);
        $entityManager->flush();
        $this->addFlash('success','Запись успешно удалена');
        return $this->redirectToRoute('admin_course_edit',['id' => $courseId]);
    }
}

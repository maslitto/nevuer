<?php

namespace App\Controller\Front;

use App\Entity\User\ResetPassword;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User\User;
use App\Repository\User\ResetPasswordRepository;
use App\Repository\User\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AuthController extends AbstractController
{
    private $sender;
    public function __construct(String $sender)
    {
        $this->sender = $sender;
    }
    /**
     * @Route("/login/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('front/auth/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/ajax-login/", name="ajax_login")
     */
    public function ajaxLogin(Request $request)
    {
        $user = $this->getUser();

        return $this->json(array(
            'username' => $user->getUsername(),
            'roles' => $user->getRoles(),
        ));
    }

    /**
     * @Route("/register/", name="register")
     */
    public function register(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder, \Swift_Mailer $mailer): Response
    {
        do {
            $hash = uniqid();
        } while ($user = $userRepository->findOneBy(['hash' => $hash]));
        $data = json_decode($request->getContent(), true);
        if($user = $userRepository->findOneBy(['email' => $data['username']])){
            return $this->json([
                'errors' => [
                    'username' => 'Такой e-mail уже зарегистрирован',
                ]
            ],Response::HTTP_UNPROCESSABLE_ENTITY);
        } else {
            $user = new User();
            $user->setEmail($data['username']);
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $data['password']
                )
            );
            $user->setRoles(['ROLE_USER']);
            $user->setActive(true);
            $user->setHash($hash);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            /*$message = (new \Swift_Message('Завершение регистрации на сайте revuer.ru'))
                ->setFrom($this->sender)
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView('front/email/register.twig', [
                        'hash' => $hash,
                        //'server' => $_SERVER
                    ]), 'text/html'
                );

            $mailer->send($message);*/
            if($request->isXmlHttpRequest()){
                return $this->json(['success' => true]);
            }
            return $this->json([
                'success' => true,
            ]);
        }

    }

    /**
     * @Route("/registration/confirm/{hash}", name="confirm_regsitration")
     */
    public function confirmRegistration(string $hash, UserRepository $userRepository,Request $request, EventDispatcherInterface $eventDispatcher): Response
    {
        if($user = $userRepository->findOneBy(['hash' => $hash,'active' => false])){
            $user->setActive(true);
            $user->setHash(NULL);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            //Handle getting or creating the user entity likely with a posted form
            // The third parameter "main" can change according to the name of your firewall in security.yml
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);

            // If the firewall name is not main, then the set value would be instead:
            // $this->get('session')->set('_security_XXXFIREWALLNAMEXXX', serialize($token));
            $this->get('session')->set('_security_main', serialize($token));

            // Fire the login event manually
            $event = new InteractiveLoginEvent($request, $token);
            $eventDispatcher->dispatch("security.interactive_login", $event);
            $this->addFlash('success','Вы успешно подтвердили e-mail');
            return $this->redirectToRoute('index');
        }
        else {
            throw $this->createNotFoundException('Неверная ссылка');
        }


    }
    /**
     * @Route("/forget-password/", name="forget_password")
     */
    public function forgetPassword(Request $request, UserRepository $userRepository, \Swift_Mailer $mailer): Response
    {

        if($request->isMethod('GET')){
            return $this->render('front/auth/forget_password.html.twig');
        } else {
            $data = json_decode($request->getContent(), true);
            if($user = $userRepository->findOneBy(['email' => $data['email']])){
                $resetPassword = new ResetPassword();
                $resetPassword->setUser($user);
                $resetPassword->setHash(uniqid());
                $em = $this->getDoctrine()->getManager();
                $em->persist($resetPassword);
                $em->flush();

                $message = (new \Swift_Message('Восстановление пароля'))
                    ->setFrom($this->sender)
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            'front/email/reset_password.html.twig',
                            ['hash' => $resetPassword->getHash()]
                        ),
                        'text/html'
                    );

                $mailer->send($message);
                return $this->json(['success' => true]);
            }
            else{
                return $this->json([
                    'errors' => [
                        'email'  => 'Пользователь с таким e-mail не найден']
                ],Response::HTTP_UNPROCESSABLE_ENTITY);

            }
            //return $this->render('front/auth/forget_password.html.twig',['sended' => true]);
        }

    }

    /**
     * @Route("/reset-password/{hash}/", name="reset_password")
     */
    public function resetPassword(
        string $hash,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        ResetPasswordRepository $resetPasswordRepository,
        Request $request
    ): Response
    {
        $resetPassword = $resetPasswordRepository->findOneBy(['hash' => $hash]);
        if($request->isMethod('GET')) {
            if ($resetPassword) {
                return $this->render('front/auth/reset_password.html.twig', ['hash' => $hash]);
            } else {
                throw $this->createNotFoundException('Неверная ссылка');
            }
        } else {
            $data = json_decode($request->getContent(), true);
            if($data['password']!==$data['confirmPassword']){
                return $this->json([
                    'errors' => [
                        'confirmPassword' => 'Введенные пароли не совпадают'
                    ]
                ],Response::HTTP_UNPROCESSABLE_ENTITY);
            } else {
                $user = $userRepository->findOneBy(['id' => $resetPassword->getUser()->getId()]);
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $data['password']
                    )
                );
                $em = $this->getDoctrine()->getManager();
                $em->remove($resetPassword);
                $em->persist($user);
                $em->flush();
                return $this->json([
                    'success' => true
                ],Response::HTTP_OK);
                //return $this->redirectToRoute('login');
            }
        }
    }
}

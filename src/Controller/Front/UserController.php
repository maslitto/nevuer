<?php

namespace App\Controller\Front;

use App\Entity\User\User;
use App\Entity\User\UserProfile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * User controller
 *
 * @Route("/user")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/user/profile/", name="user_profile_index")
     */
    public function index()
    {
        $user = $this->getUser();

        return $this->render('front/user/index.html.twig',[
            'user' => $user,
            'profile' => $user->getProfile()
        ]);
    }

    /**
     * @Route("/save-profile/", name="user_save_profile")
     */
    public function saveProfile(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        /**@var \App\Entity\User\User $user */
        $user = $this->getUser();

        $profile = $user->getProfile();
        if(!$profile){
            $profile = new UserProfile();
        }
        /** @var UserProfile $profile */
        $profile->setUser($user);
        $profile->setName($data['name']);
        $profile->setSecondName($data['secondName']);
        $profile->setBirthday(new \DateTime($data['birthday']));
        $profile->setCity($data['city']);
        $profile->setProfession($data['profession']);
        $profile->setAbout($data['about']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($profile);
        $em->flush();

        return $this->json([
            'success' => true
        ]);
    }

    /**
     * @Route("/upload-avatar/", name="user_upload_avatar")
     */
    public function uploadAvatar(Request $request)
    {
        $em =$this->getDoctrine()->getManager();
        $fileSystem = new Filesystem();
        /** @var User $user */
        $user = $this->getUser();
        $profile = $user->getProfile();
        $file = $request->files->get('file');
        //dump($files);die();
        $fileName = uniqid().'.'.$file->guessClientExtension();
        $filePath = UserProfile::AVATARS_PATH.$fileName;
        try {
            $file->move($this->getParameter('publicPath').UserProfile::AVATARS_PATH, $fileName);
        } catch (FileException $e) {
            return $this->json([
                'message' => 'Ошибка при загрузке файлов! '. $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
        if($profile->getAvatar()){
            $fileSystem->remove($this->getParameter('publicPath').$profile->getAvatar());
        }
        $profile->setAvatar($filePath);
        $em->persist($profile);

        $em->flush();

        return $this->json([
            'success' => true,
            'avatar' => $filePath
        ]);
    }

}

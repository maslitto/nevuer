<?php

namespace App\Controller\Front;

use App\Entity\ActivationRequest;
use App\Entity\Course;
use App\Entity\User\UserProfile;
use App\Repository\ActivationRequestRepository;
use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * User controller
 *
 * @Route("/activation-request")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class ActivationRequestController extends AbstractController
{
    /**
     * @Route("/", name="post_activation_request")
     */
    public function requestActivation(Request $request, CourseRepository $courseRepository)
    {
        $id = $request->get('id',null);
        /**@var \App\Entity\User\User $user */
        $user = $this->getUser();

        $course = $courseRepository->findOneBy(['id' => $id]);
        $activationRequest = new ActivationRequest();

        $activationRequest->setCourse($course);
        $activationRequest->setUser($user);
        $activationRequest->setActive(false);

        $em = $this->getDoctrine()->getManager();
        $em->persist($activationRequest);
        $em->flush();

        return $this->json([
            'success' => true,
            'message' => 'Идет активация'
        ]);
    }

}

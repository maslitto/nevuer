<?php

namespace App\Controller\Front;

use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(CourseRepository $courseRepository)
    {
        /*$encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);*/
        //$serializer = $this->get('serializer');

        $user = $this->getUser();
        $courses = $courseRepository->findBy(['active' => true]);
        return $this->render('front/index/index.html.twig', [
            'user' => $user,
            'courses' => $courses,
        ]);
    }
}

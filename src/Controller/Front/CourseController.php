<?php

namespace App\Controller\Front;


use App\Entity\Course;
use App\Entity\Task;
use App\Entity\Week;
use App\Repository\WeekRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * User controller
 *
 * @Route("/course")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class CourseController extends AbstractController
{
    /**
     * @Route("/{id}/{number}/", name="course_index", defaults={"number": 1})
     *
     */
//@ParamConverter("week", options={"mapping": {"number": "number","course_id":"id"}})
    public function index(Course $course, int $number, WeekRepository $weekRepository)
    {
        if(!$course->hasUser($this->getUser())){
            throw new AccessDeniedHttpException('Доступ запрещён! Вы не зарегистрировались на курс .');
        }
        $week = $weekRepository->findOneBy(['course' => $course, 'number' => $number]);
        if(!$week->isEnabled()){
            throw new AccessDeniedHttpException('Доступ запрещён! Неделя ещё не началась .');
        }
        $em = $this->getDoctrine()->getManager();
        /** @var Task[] $tasks */
        $tasks = $week->getTasks();
        $messages = 0;
        foreach ($tasks as $task) {
            if($answer = $task->getAnswerFromUser($this->getUser())){
                if($answer->getUserNew()){
                    $messages = $messages + 1;
                    $answer->setUserNew(false);
                    $em->persist($answer);
                }
            }
        }
        $em->flush();
        return $this->render('front/course/index.html.twig',[
            'course' => $course,
            'openWeek' => $week,
            'messages' => $messages
        ]);
    }

}

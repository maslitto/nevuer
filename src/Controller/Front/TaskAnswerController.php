<?php

namespace App\Controller\Front;

use App\Entity\ActivationRequest;
use App\Entity\AnswerImage;
use App\Entity\Course;
use App\Entity\Task;
use App\Entity\TaskAnswer;
use App\Entity\User\UserProfile;
use App\Repository\ActivationRequestRepository;
use App\Repository\AnswerImageRepository;
use App\Repository\CourseRepository;
use App\Repository\TaskAnswerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * User controller
 *
 * @Route("/task-answer")
 */
class TaskAnswerController extends AbstractController
{
    /**
     * @Route("/{id}/", name="post_task_answer")
     */
    public function postTaskAnswer(Task $task, Request $request,TaskAnswerRepository $taskAnswerRepository)
    {
        $user = $this->getUser();
        if(!$user){
            return $this->json(['message' => 'Произошла ошибка при отправке. Перезагрузите страницу!'],Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $em = $this->getDoctrine()->getManager();
        /**@var \App\Entity\User\User $user */
        $user = $this->getUser();
        $taskAnswer = $taskAnswerRepository->findOneBy(['user' => $user, 'task' => $task]);
        if(!$taskAnswer) {
            $taskAnswer = new TaskAnswer();
        }
        $taskAnswer->setAdminNew(true);
        $taskAnswer->setStatus(TaskAnswer::TASK_PENDING);

        $taskAnswer->setUser($user);
        $taskAnswer->setTask($task);
        $taskAnswer->setText($request->get('text'));

        $em->persist($taskAnswer);
        $em->flush();
        $response = [
            'success' => true,
            'message' => '<p>Задание отправлено на проверку.</p><br><p>Вы можете редактировать ответ до наступления дедлайна.</p>'
        ];
        return $this->json($response);
    }
    /**
     * @Route("/{id}/images/add/", name="task_answer_add_image")
     */
    public function postTaskAnswerImages(Task $task, Request $request,TaskAnswerRepository $taskAnswerRepository)
    {
        $user = $this->getUser();
        if(!$user){
            return $this->json(['message' => 'Произошла ошибка при отправке. Перезагрузите страницу!'],Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $em = $this->getDoctrine()->getManager();
        /**@var \App\Entity\User\User $user */
        $user = $this->getUser();
        if(!$taskAnswer = $taskAnswerRepository->findOneBy(['user' => $user, 'task' => $task])){
            $taskAnswer = new TaskAnswer();
        }
        $taskAnswer->setAdminNew(true);
        $taskAnswer->setStatus(TaskAnswer::TASK_PENDING);
        /** @var UploadedFile[] $files */
        $file = $request->files->get('file');
        if($file){
            $fileName = uniqid().'.'.$file->guessClientExtension();
            $filePath = AnswerImage::FILES_PATH.$fileName;
            $fullPath = $this->getParameter('publicPath').AnswerImage::FILES_PATH.$fileName;
            try {
                $file->move($this->getParameter('publicPath').AnswerImage::FILES_PATH, $fileName);
            } catch (FileException $e) {
                return $this->json([
                    'message' => 'Ошибка при загрузке файлов! '. $e->getMessage()
                ], Response::HTTP_BAD_REQUEST);
            }
            $answerImage = new AnswerImage();
            $answerImage->setAnswer($taskAnswer);
            $answerImage->setFilename($fileName);
            $answerImage->setFilepath($filePath);
            $answerImage->setFullPath($fullPath);
            $em->persist($answerImage);
        }

        $em->persist($taskAnswer);
        $em->flush();
        $response = [
            'success' => true,
            'message' => '<p>Задание отправлено на проверку.</p><br><p>Вы можете редактировать ответ до наступления дедлайна.</p>'
        ];
        return $this->json($response);
    }
    /**
     * @Route("/images/remove/", name="task_answer_remove_image")
     */
    public function removeTaskImage(Request $request,AnswerImageRepository $answerImageRepository)
    {
        $user = $this->getUser();
        if(!$user){
            return $this->json(['message' => 'Произошла ошибка при отправке. Перезагрузите страницу!'],Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $data = json_decode($request->getContent());
        /** @var AnswerImage $answerImage */
        $answerImage = $answerImageRepository->findOneBy(['id' => $data->id]);
        $em = $this->getDoctrine()->getManager();
        $fileSystem = new Filesystem();
        if($fileSystem->exists($answerImage->getFullPath())){
            $fileSystem->remove($answerImage->getFullPath());
        }
        $em->remove($answerImage);
        $em->flush();
        $response = [
            'success' => true,
        ];
        return $this->json($response);
    }

}

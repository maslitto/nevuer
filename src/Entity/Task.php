<?php

namespace App\Entity;

use App\Entity\User\User;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    const PER_PAGE = 50;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $recommendation;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var \DateTime|NULL
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP", "update": "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deadline;

    /**
     * @var Course|null
     *
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="tasks")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", nullable=true)
     */
    private $course;

    /**
     * @var Week|null
     *
     * @ORM\ManyToOne(targetEntity="Week", inversedBy="tasks")
     * @ORM\JoinColumn(name="week_id", referencedColumnName="id", nullable=true)
     */
    private $week;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TaskAnswer", mappedBy="task")
     */
    private $answers;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->deadline = new \DateTime();
        $this->answers = new ArrayCollection();
    }

    public function isEnabled()
    {
        /*$weekTasks = $this->week->getTasks();
        if($this->week->getNumber() == 1 && $weekTasks->first() == $this){
            return true;
        }
        $now = Carbon::now();
        if($now->diffInHours($this->deadline)< 24 || $now > $this->deadline){
            return true;
        } else {
            return false;
        }*/
        return true;
    }
    public function isCurrent()
    {
        $now = Carbon::now();
        if($now->diffInHours($this->deadline) < 24 && $now < $this->deadline){
            return true;
        }
        elseif(!$this->hasCurrent() && $this->id == $this->week->getFirstTask()->getId()){
            return true;
        }
        else {
            return false;
        }
    }

    public function hasCurrent()
    {
        $now = Carbon::now();

        $tasks = $this->week->getTasks();
        foreach ($tasks as $task){
            if($now->diffInHours($task->deadline) < 24 && $now < $task->deadline){
                return true;
            }
        }
        return false;
    }
    public function showForm(User $user): bool
    {
        /*if($this->isExpired($user)){
            return false;
        }*/
        if(!$this->isCurrent()){
            return false;
        }
        /** @var TaskAnswer|null $taskAnswer */
        $taskAnswer = $this->getAnswerFromUser($user);
        if (!$taskAnswer){
            return true;
        } else {
            if($taskAnswer->getStatus() == TaskAnswer::TASK_PENDING
                || $taskAnswer->getStatus() == TaskAnswer::TASK_CREATED
                || $taskAnswer->getStatus() == TaskAnswer::TASK_REWORK
            ){
                return true;
            } else {
                return false;
            }
        }
    }
    public function isExpired(User $user)
    {
        if((Carbon::now() > $this->deadline) && !$this->getAnswerFromUser($user)){
            return true;
        } elseif((Carbon::now() > $this->deadline) && $this->getAnswerFromUser($user)->getStatus() == TaskAnswer::TASK_PENDING) {
            return false;
        } else {
            return false;
        }
    }

    public function getAnswerFromUser(User $user): ?TaskAnswer
    {
        /** @var TaskAnswer[] $userAnswers */
        $userAnswers = $user->getAnswers();
        $taskAnswers = $this->answers;
        foreach ($userAnswers as $userAnswer){
            if($taskAnswers->contains($userAnswer))
                return $userAnswer;
        }
        return null;
    }

    public function getDay()
    {
        $carbon = Carbon::createFromTimestamp($this->deadline->getTimestamp())->locale('ru');
        return ucfirst($carbon->getTranslatedDayName());
    }
    public function getLeftToDeadline()
    {
        $deadline = Carbon::createFromTimestamp($this->deadline->getTimestamp())->locale('ru');
        $now = Carbon::now();

        return $now->diff($deadline)->format('%dд %Hч %Iм');
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getRecommendation(): ?string
    {
        return $this->recommendation;
    }

    public function setRecommendation(?string $recommendation): self
    {
        $this->recommendation = $recommendation;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|NULL
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|NULL $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return Course|null
     */
    public function getCourse(): ?Course
    {
        return $this->course;
    }

    /**
     * @param Course|null $course
     */
    public function setCourse(?Course $course): void
    {
        $this->course = $course;
    }


    public function getDeadline(): ?\DateTimeInterface
    {
        return $this->deadline;
    }

    public function setDeadline(?\DateTimeInterface $deadline): self
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * @return Week|null
     */
    public function getWeek(): ?Week
    {
        return $this->week;
    }

    /**
     * @param Week|null $week
     */
    public function setWeek(?Week $week): void
    {
        $this->week = $week;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Collection|TaskAnswer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(TaskAnswer $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setTask($this);
        }

        return $this;
    }

    public function removeAnswer(TaskAnswer $answer): self
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
            // set the owning side to null (unless already changed)
            if ($answer->getTask() === $this) {
                $answer->setTask(null);
            }
        }

        return $this;
    }


}

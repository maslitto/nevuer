<?php

namespace App\Entity\User;

use App\Entity\Course;
use App\Entity\TaskAnswer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * @ORM\Entity(repositoryClass="App\Repository\User\UserRepository")
 * @UniqueEntity(fields={"email"}, message="Аккаунт с таким e-mail уже существует")
 */
class User implements UserInterface
{
    const PER_PAGE = 20;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"default"})
     */
    private $id;

    /**
     * @Groups({"default"})
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @Groups({"default"})
     * @var bool|NULL
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     *
     * @var string|NULL
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $hash;

    /**
     * @Groups({"default"})
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @Groups({"default"})
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @Groups({"default"})
     * @var \DateTime|NULL
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP", "update": "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * One User has One UserProfile.
     *
     * @var UserProfile|NULL
     * @Groups({"default"})
     * @ORM\OneToOne(targetEntity="App\Entity\User\UserProfile", mappedBy="user")
     */
    private $profile;

    /**
     * @var ArrayCollection|PersistentCollection
     * Many users may have many courses .
     *
     * @ORM\ManyToMany(targetEntity="\App\Entity\Course",inversedBy="users")
     * @ORM\JoinTable(name="courses_users")
     *
     */
    private $courses;

    /**
     * @var ArrayCollection|PersistentCollection
     * @ORM\OneToMany(targetEntity="\App\Entity\ActivationRequest", mappedBy="user")
     */
    private $activationRequests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TaskAnswer", mappedBy="user")
     */
    private $answers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TaskAnswer", mappedBy="editor")
     */
    private $editorAnswers;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->courses = new ArrayCollection();
        $this->activationRequests = new ArrayCollection();
        $this->answers = new ArrayCollection();
        $this->editorAnswers = new ArrayCollection();
    }

    public function addCourse(Course $course)
    {
        if (!$this->hasCourse($course)) {
            $this->courses->add($course);
        }
        return $this;
    }
    public function removeCourse(Course $course)
    {
        if ($this->hasCourse($course)) {
            $this->courses->removeElement($course);
        }
        return $this;
    }

    public function hasCourse(Course $course) : bool
    {
        if ($this->courses->contains($course)){
            return true;
        } else {
            return false;
        }
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return bool|NULL
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|NULL $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return NULL|string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param NULL|string $hash
     */
    public function setHash(?string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|NULL
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|NULL $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return UserProfile|NULL
     */
    public function getProfile(): ?UserProfile
    {
        return $this->profile;
    }

    /**
     * @param UserProfile|NULL $profile
     */
    public function setProfile(?UserProfile $profile): void
    {
        $this->profile = $profile;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * @param ArrayCollection|PersistentCollection $courses
     */
    public function setCourses($courses): void
    {
        $this->courses = $courses;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getActivationRequests()
    {
        return $this->activationRequests;
    }

    /**
     * @param ArrayCollection|PersistentCollection $activationRequests
     */
    public function setActivationRequests($activationRequests): void
    {
        $this->activationRequests = $activationRequests;
    }

    /**
     * @return Collection|TaskAnswer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(TaskAnswer $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setUser($this);
        }

        return $this;
    }

    public function removeAnswer(TaskAnswer $answer): self
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
            // set the owning side to null (unless already changed)
            if ($answer->getUser() === $this) {
                $answer->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TaskAnswer[]
     */
    public function getEditorAnswers(): Collection
    {
        return $this->editorAnswers;
    }

    public function addEditorAnswer(TaskAnswer $editorAnswer): self
    {
        if (!$this->editorAnswers->contains($editorAnswer)) {
            $this->editorAnswers[] = $editorAnswer;
            $editorAnswer->setEditor($this);
        }

        return $this;
    }

    public function removeEditorAnswer(TaskAnswer $editorAnswer): self
    {
        if ($this->editorAnswers->contains($editorAnswer)) {
            $this->editorAnswers->removeElement($editorAnswer);
            // set the owning side to null (unless already changed)
            if ($editorAnswer->getEditor() === $this) {
                $editorAnswer->setEditor(null);
            }
        }

        return $this;
    }

}

<?php

namespace App\Entity;

use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WeekRepository")
 */
class Week
{
    const PER_PAGE = 20;
    const DAY_SUNDAY = 7;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var \DateTime|NULL
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP", "update": "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @var Course|null
     *
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="weeks")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", nullable=true)
     */
    private $course;
    /**
     *
     * @ORM\OneToMany(targetEntity="Task", mappedBy="week")
     * @ORM\OrderBy({"deadline":"ASC"})
     */
    private $tasks;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->tasks = new ArrayCollection();
    }
    public function isEnabled(){
        if($this->number == 0){
            return true;
        }
        $now = Carbon::now();
        $currentWeekNumber = $this->course->getCurrentWeekNumber();
        $dayNumber = $now->isoWeekday();
        $currentHour = (int) $now->format('H');
        if($this->number <= $currentWeekNumber || $currentWeekNumber + 1 == $this->course->getLastWeekNumber()){
            return true;
        } elseif($dayNumber == self::DAY_SUNDAY && $currentWeekNumber + 1 == $this->number && $currentHour >= 12){
            return true;
        } else {
            return false;
        }
        /*$courseStart = Carbon::createFromTimestamp($this->course->getRealDateStart()->getTimestamp());
        $now = Carbon::now();
        //dump($courseStart->year.'-'.$courseStart->month.'-'.$courseStart->day);die();
        $weekStartDate = $courseStart->addWeeks($this->number);
        if($now < $weekStartDate->subDay()){
            return false;
        } else {
            return true;
        }*/
    }

    public function isCurrent()
    {
        return $this->number == $this->course->getCurrentWeekNumber();
    }
    public function getFirstTask()
    {
        return $this->tasks->first();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int|null $number
     */
    public function setNumber(?int $number): void
    {
        $this->number = $number;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|NULL
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|NULL $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return Course|null
     */
    public function getCourse(): ?Course
    {
        return $this->course;
    }

    /**
     * @param Course|null $course
     */
    public function setCourse(?Course $course): void
    {
        $this->course = $course;
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param mixed $tasks
     */
    public function setTasks($tasks): void
    {
        $this->tasks = $tasks;
    }



}

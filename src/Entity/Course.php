<?php

namespace App\Entity;

use App\Entity\User\User;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 */
class Course
{
    const PER_PAGE = 20;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateStart;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $teachers;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     *
     * @ORM\OneToMany(targetEntity="Task", mappedBy="course")
     * @ORM\OrderBy({"deadline" = "ASC"})
     */
    private $tasks;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var \DateTime|NULL
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP", "update": "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;
    /**
     *
     * @ORM\OneToMany(targetEntity="Week", mappedBy="course")
     * @ORM\OrderBy({"number" = "ASC"})
     */
    private $weeks;

    /**
     * @var ArrayCollection|PersistentCollection
     * @ORM\OneToMany(targetEntity="ActivationRequest", mappedBy="course")
     */
    private $activationRequests;

    /**
     * @var ArrayCollection|PersistentCollection
     * Many courses may have many users.
     * @ORM\ManyToMany(targetEntity="\App\Entity\User\User",mappedBy="courses")
     *
     */
    private $users;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->users = new ArrayCollection();
        $this->tasks = new ArrayCollection();
        $this->activationRequests = new ArrayCollection();
    }

    public function getRealDateStart(){
        return $this->dateStart;
        /** @var Task $firstTask */
        $firstTask = $this->tasks->first();

        if($firstTask){
            return Carbon::createFromTimestamp($firstTask->getDeadline()->getTimestamp())->subDay();
        } else {
            return $this->dateStart;
        }
        //return $this->dateStart;
    }

    public function getLastWeekNumber(): int
    {
        /** @var Task $lastTask */
        $lastTask = $this->tasks->last();
        return $lastTask->getWeek()->getNumber();
    }
    public function getCurrentWeekNumber()
    {
        $courseStart = Carbon::createFromTimestamp($this->getRealDateStart()->getTimestamp());
        //dd($courseStart);die();
        $now = Carbon::now();
        if($now < $this->getRealDateStart()){
            return 0;
        } else{
            $number = $now->diffInWeeks($courseStart); //+1
            if($number <= 8) return $number;
            else return 0;
        }
    }
    public function hasUnactivatedUser(User $user)
    {
        $usersRequests = $user->getActivationRequests();
        $courseRequests = $this->activationRequests;
        foreach ($usersRequests as $usersRequest){
            if($courseRequests->contains($usersRequest))
                return true;
        }
        return false;
    }
    public function addUser(User $user)
    {
        if (!$this->hasUser($user)) {
            $this->users->add($user);
        }
        return $this;
    }

    public function removeUser(User $user)
    {
        if ($this->hasUser($user)) {
            $this->users->removeElement($user);
        }
        return $this;
    }

    public function hasUser(User $user) : bool
    {
        if ($this->users->contains($user)){
            return true;
        } else {
            return false;
        }
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateStart(): ?\DateTime
    {
        return $this->dateStart;
    }

    /**
     * @param \DateTime|null $dateStart
     */
    public function setDateStart(?\DateTime $dateStart): void
    {
        $this->dateStart = $dateStart;
    }


    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param mixed $tasks
     */
    public function setTasks($tasks): void
    {
        $this->tasks = $tasks;
    }

    /**
     * @return mixed
     */
    public function getWeeks()
    {
        return $this->weeks;
    }

    /**
     * @param mixed $weeks
     */
    public function setWeeks($weeks): void
    {
        $this->weeks = $weeks;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection|PersistentCollection $users
     */
    public function setUsers($users): void
    {
        $this->users = $users;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|NULL
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|NULL $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return null|string
     */
    public function getTeachers(): ?string
    {
        return $this->teachers;
    }

    /**
     * @param null|string $teachers
     */
    public function setTeachers(?string $teachers): void
    {
        $this->teachers = $teachers;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getActivationRequests()
    {
        return $this->activationRequests;
    }

    /**
     * @param ArrayCollection|PersistentCollection $activationRequests
     */
    public function setActivationRequests($activationRequests): void
    {
        $this->activationRequests = $activationRequests;
    }

}

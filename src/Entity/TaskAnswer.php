<?php

namespace App\Entity;

use App\Entity\User\User;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskAnswerRepository")
 */
class TaskAnswer
{
    const PER_PAGE = 50;
    const TASK_PENDING = 0;
    const TASK_OK = 1;
    const TASK_FAILED = 2;
    const TASK_REWORK = 3;
    const TASK_CREATED = 4;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task", inversedBy="answers")
     */
    private $task;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="answers")
     */
    private $user;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var \DateTime|NULL
     * @ORM\Column(type="datetime", nullable=true,options={"default": "CURRENT_TIMESTAMP", "update": "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AnswerImage", mappedBy="answer")
     */
    private $answerImages;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $adminNew;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $userNew;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="editorAnswers")
     */
    private $editor;

    public function getJsonImages()
    {
        if($this->answerImages->count()){
            foreach ($this->answerImages as $k => $answerImage) {
                $item = [
                    'path' => $answerImage->getFilepath(),
                    'caption' => $answerImage->getId(),
                    'highlight' => 0,

                ];
                if($k == 0){
                    $item['highlight'] = 1;

                }
                $response[] = $item;
            }
            return json_encode($response);
        } else {
            return json_encode([]);
        }
    }
    public function hasImages(): bool
    {
        if($this->answerImages->count() > 0){
            return true;
        } else {
            return false;
        }
    }
    public function __construct()
    {
        $this->status = self::TASK_PENDING;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->answerImages = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTask(): ?Task
    {
        return $this->task;
    }

    public function setTask(?Task $task): self
    {
        $this->task = $task;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|NULL
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|NULL $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|AnswerImage[]
     */
    public function getAnswerImages(): Collection
    {
        return $this->answerImages;
    }

    public function addAnswerImage(AnswerImage $answerImage): self
    {
        if (!$this->answerImages->contains($answerImage)) {
            $this->answerImages[] = $answerImage;
            $answerImage->setAnswer($this);
        }

        return $this;
    }

    public function removeAnswerImage(AnswerImage $answerImage): self
    {
        if ($this->answerImages->contains($answerImage)) {
            $this->answerImages->removeElement($answerImage);
            // set the owning side to null (unless already changed)
            if ($answerImage->getAnswer() === $this) {
                $answerImage->setAnswer(null);
            }
        }

        return $this;
    }

    public function getAdminNew(): ?bool
    {
        return $this->adminNew;
    }

    public function setAdminNew(?bool $adminNew): self
    {
        $this->adminNew = $adminNew;

        return $this;
    }

    public function getUserNew(): ?bool
    {
        return $this->userNew;
    }

    public function setUserNew(?bool $userNew): self
    {
        $this->userNew = $userNew;

        return $this;
    }

    public function getEditor(): ?User
    {
        return $this->editor;
    }

    public function setEditor(?User $editor): self
    {
        $this->editor = $editor;

        return $this;
    }


}